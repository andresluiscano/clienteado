// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

//CSS
/*import '../assets/global/plugins/font-awesome/css/font-awesome.min.css'
import '../assets/global/plugins/simple-line-icons/simple-line-icons.min.css'
import '../assets/global/plugins/bootstrap/css/bootstrap.min.css'
import '../assets/global/plugins/uniform/css/uniform.default.css'
import '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'
import '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css'
import '../assets/global/css/components.min.css'
import '../assets/global/css/plugins.min.css'

import '../assets/layouts/layout/css/layout.min.css'
import '../assets/layouts/layout/css/themes/darkblue.min.css'
import '../assets/layouts/layout/css/custom.min.css'

//JS
import '../assets/global/plugins/jquery.min.js'
import  '../assets/global/plugins/bootstrap/js/bootstrap.min.js'
import  '../assets/global/plugins/js.cookie.min.js'
import  '../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'
import  '../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'
import  '../assets/global/plugins/jquery.blockui.min.js'
import  '../assets/global/plugins/uniform/jquery.uniform.min.js'
import  '../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'
//import  '../assets/global/plugins/moment.min.js'
import  '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js'
import  '../assets/global/plugins/morris/morris.min.js'
import  '../assets/global/plugins/amcharts/amcharts/amcharts.js'
import  '../assets/global/plugins/amcharts/amcharts/serial.js'
import  '../assets/global/plugins/amcharts/amcharts/pie.js'
import  '../assets/global/plugins/amcharts/amcharts/radar.js'
import  '../assets/global/plugins/amcharts/amcharts/themes/light.js'
import  '../assets/global/plugins/amcharts/amcharts/themes/patterns.js'
import  '../assets/global/plugins/amcharts/amcharts/themes/chalk.js'
import  '../assets/global/plugins/amcharts/ammap/ammap.js'
import  '../assets/global/plugins/amcharts/ammap/maps/js/worldLow.js'
import  '../assets/global/plugins/amcharts/amstockcharts/amstock.js'
//import  '../assets/pages/scripts/dashboard.min.js'
import  '../assets/global/scripts/app.min.js'
/*import  '../assets/pages/scripts/dashboard.min.js'
import  '../assets/layouts/layout/scripts/layout.min.js'
import  '../assets/layouts/layout/scripts/demo.min.js'
import  '../assets/layouts/global/scripts/quick-sidebar.min.js'*/



Vue.config.productionTip = false

//Firebase
import VueFire from 'vuefire'
Vue.config.productionTip = false

Vue.use(VueFire);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
})

