import Vue from 'vue'
import Router from 'vue-router'
import PanelUsuarios from '@/components/PanelUsuariosView'
import AgendaView from '@/components/AgendaView'
import LoginView from '@/components/LoginView'
import UsuariosView from '@/components/UsuariosView'
import TiposPedidosView from '@/components/TiposPedidosView'
import PedidosView from '@/components/PedidosView'
import * as VueGoogleMaps from "vue2-google-maps";
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue);
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyA5PA8byVp83QHJpxrfN_ejtmriq6PYtLw",
    libraries: "places" // necessary for places input
  }
});

Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/PanelUsuarios',
      name: 'PanelUsuarios',
      component: PanelUsuarios
    },
    {
      path: '/Agenda',
      name: 'AgendaView',
      component: AgendaView
    },
    {
      path: '/Login',
      name: 'LoginView',
      component: LoginView
    },
    {
      path: '/TiposPedidos',
      name: 'TiposPedidosView',
      component: TiposPedidosView
    },
    {
      path: '/Pedidos',
      name: 'PedidosView',
      component: PedidosView
    },
    {
      path: '/Usuarios',
      name: 'UsuariosView',
      component: UsuariosView
    },
  ]
})
